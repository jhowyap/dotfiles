#!/bin/bash
############################
# source.sh
# This script loads symlink in home directory
############################

########## Variables


files="bashrc zshrc bash_profile vimrc gitconfig"   

##########

# source all file in files
for file in $files; do
    source ~/.$file
done
