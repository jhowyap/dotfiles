#!/bin/bash
############################
# make_symlink.sh
# This script creates symlinks from the home directory 
# to any desired dotfiles in ~/repos/dotfiles
############################

########## Variables

dir=$(pwd)                 # dotfiles directory
olddir=$dir/dotfiles_old             # old dotfiles backup directory
# list of files/folders to symlink in homedir
files="bashrc vimrc zshrc bash_profile gitconfig"   

##########

# backup dotfiles
echo "Creating $olddir for backup of any existing dotfiles in ~"
mkdir -p $olddir
echo "...done"

# change to the dotfiles directory
echo "Changing to the $dir directory"
cd $dir
echo "...done"

# move any existing dotfiles in homedir to dotfiles_old directory, then create symlinks 
for file in $files; do
    echo "Moving any existing dotfiles from ~ to $olddir"
    mv ~/.$file $olddir
    echo "Creating symlink to $file in home directory."
    ln -s $dir/$file ~/.$file
    source ~/.$file
done
