Welcome to My Dotfiles Repo

-------------------------------------

This repo contains the dotfiles configuration for my terminal.
1. bashrc
    - setup local environment variable as well as terminal configuration
    - different display when one is in git repository

2. vimrc
    - setup the environment for vim

3. bash_profile
4. zshrc
5. gitconfig
